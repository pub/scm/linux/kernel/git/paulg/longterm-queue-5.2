From ac9842f0fa30b6e77e7da323e67c4cfc81cdd010 Mon Sep 17 00:00:00 2001
From: Jeroen Hofstee <jhofstee@victronenergy.com>
Date: Tue, 24 Sep 2019 18:45:38 +0000
Subject: [PATCH] can: rx-offload: can_rx_offload_irq_offload_timestamp():
 continue on error

commit c2a9f74c9d18acfdcabd3361adc7eac82c537a66 upstream.

In case of a resource shortage, i.e. the rx_offload queue will overflow
or a skb fails to be allocated (due to OOM),
can_rx_offload_offload_one() will call mailbox_read() to discard the
mailbox and return an ERR_PTR.

However can_rx_offload_irq_offload_timestamp() bails out in the error
case. In case of a resource shortage all mailboxes should be discarded,
to avoid an IRQ storm and give the system some time to recover.

Since can_rx_offload_irq_offload_timestamp() is typically called from a
while loop, all message will eventually be discarded. So let's continue
on error instead to discard them directly.

Signed-off-by: Jeroen Hofstee <jhofstee@victronenergy.com>
Signed-off-by: Marc Kleine-Budde <mkl@pengutronix.de>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/net/can/rx-offload.c b/drivers/net/can/rx-offload.c
index 3f5e040f0c71..2ea8676579a9 100644
--- a/drivers/net/can/rx-offload.c
+++ b/drivers/net/can/rx-offload.c
@@ -216,7 +216,7 @@ int can_rx_offload_irq_offload_timestamp(struct can_rx_offload *offload, u64 pen
 
 		skb = can_rx_offload_offload_one(offload, i);
 		if (IS_ERR_OR_NULL(skb))
-			break;
+			continue;
 
 		__skb_queue_add_sort(&skb_queue, skb, can_rx_offload_compare);
 	}
-- 
2.7.4

