From ddbb9ee401b8c3e3833020037e0dd858b005e855 Mon Sep 17 00:00:00 2001
From: James Sewart <jamessewart@arista.com>
Date: Tue, 10 Dec 2019 16:25:40 -0600
Subject: [PATCH] PCI: Add DMA alias quirk for PLX PEX NTB

commit 7b90dfc4873b87c468cc6046538f46a531c1d785 upstream.

The PLX PEX NTB forwards DMA transactions using Requester IDs that don't
exist as PCI devices.  The devfn for a transaction is used as an index into
a lookup table storing the origin of a transaction on the other side of the
bridge.

Alias all possible devfns to the NTB device so that any transaction coming
in is governed by the mappings for the NTB.

Signed-off-by: James Sewart <jamessewart@arista.com>
Signed-off-by: Bjorn Helgaas <bhelgaas@google.com>
Reviewed-by: Logan Gunthorpe <logang@deltatee.com>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/pci/quirks.c b/drivers/pci/quirks.c
index f79e9c1e9c54..0fca7700046e 100644
--- a/drivers/pci/quirks.c
+++ b/drivers/pci/quirks.c
@@ -5179,6 +5179,21 @@ SWITCHTEC_QUIRK(0x8575);  /* PFXI 80XG3 */
 SWITCHTEC_QUIRK(0x8576);  /* PFXI 96XG3 */
 
 /*
+ * The PLX NTB uses devfn proxy IDs to move TLPs between NT endpoints.
+ * These IDs are used to forward responses to the originator on the other
+ * side of the NTB.  Alias all possible IDs to the NTB to permit access when
+ * the IOMMU is turned on.
+ */
+static void quirk_plx_ntb_dma_alias(struct pci_dev *pdev)
+{
+	pci_info(pdev, "Setting PLX NTB proxy ID aliases\n");
+	/* PLX NTB may use all 256 devfns */
+	pci_add_dma_alias(pdev, 0, 256);
+}
+DECLARE_PCI_FIXUP_HEADER(PCI_VENDOR_ID_PLX, 0x87b0, quirk_plx_ntb_dma_alias);
+DECLARE_PCI_FIXUP_HEADER(PCI_VENDOR_ID_PLX, 0x87b1, quirk_plx_ntb_dma_alias);
+
+/*
  * On Lenovo Thinkpad P50 SKUs with a Nvidia Quadro M1000M, the BIOS does
  * not always reset the secondary Nvidia GPU between reboots if the system
  * is configured to use Hybrid Graphics mode.  This results in the GPU
-- 
2.7.4

