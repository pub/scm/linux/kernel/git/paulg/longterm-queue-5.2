From bd52d452dd46283d3766725bcfa88a01e1d2cacf Mon Sep 17 00:00:00 2001
From: Bjorn Helgaas <bhelgaas@google.com>
Date: Fri, 15 May 2020 14:31:16 -0500
Subject: [PATCH] PCI/PM: Adjust pcie_wait_for_link_delay() for caller delay

commit f044baaff1eb7ae5aa7a36f1b7ad5bd8eeb672c4 upstream.

The caller of pcie_wait_for_link_delay() specifies the time to wait after
the link becomes active.  When the downstream port doesn't support link
active reporting, obviously we can't tell when the link becomes active, so
we waited the worst-case time (1000 ms) plus 100 ms, ignoring the delay
from the caller.

Instead, wait for 1000 ms + the delay from the caller.

Fixes: 4827d63891b6 ("PCI/PM: Add pcie_wait_for_link_delay()")
Signed-off-by: Bjorn Helgaas <bhelgaas@google.com>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/pci/pci.c b/drivers/pci/pci.c
index 054b01d7cd1c..11cba24e263c 100644
--- a/drivers/pci/pci.c
+++ b/drivers/pci/pci.c
@@ -4587,10 +4587,10 @@ static bool pcie_wait_for_link_delay(struct pci_dev *pdev, bool active,
 
 	/*
 	 * Some controllers might not implement link active reporting. In this
-	 * case, we wait for 1000 + 100 ms.
+	 * case, we wait for 1000 ms + any delay requested by the caller.
 	 */
 	if (!pdev->link_active_reporting) {
-		msleep(1100);
+		msleep(timeout + delay);
 		return true;
 	}
 
-- 
2.7.4

