From 1e2791448b922069b1d76cb863290c7341ff5eb5 Mon Sep 17 00:00:00 2001
From: Serge Semin <fancer.lancer@gmail.com>
Date: Fri, 14 Jun 2019 09:33:42 +0300
Subject: [PATCH] mips: Remove q-accessors from non-64bit platforms

commit 1e2791448b922069b1d76cb863290c7341ff5eb5 upstream.

There are some generic drivers in the kernel, which make use of the
q-accessors or their derivatives. While at current asm/io.h the accessors
are defined, their implementation is only applicable either for 64bit
systems, or for systems with cpu_has_64bits flag set. Obviously there
are MIPS systems which are neither of these, but still need to have
those drivers supported. In this case the solution is to define some
generic versions of the q-accessors, but with a limitation to be
non-atomic. Such accessors are defined in the
io-64-nonatomic-{hi-lo,lo-hi}.h file. The drivers which utilize the
q-suffixed IO-methods are supposed to include the header file, so
in case if these accessors aren't defined for the platform, the generic
non-atomic versions are utilized. Currently the MIPS-specific asm/io.h
file provides the q-accessors for any MIPS system even for ones, which
in fact don't support them and raise BUG() in case if any of them is
called. Due to this the generic versions of the accessors are never
used while an attempt to call the IO-methods causes the kernel BUG().
In order to fix this we need to define the q-accessors only for
the MIPS systems, which actually support them, and don't define them
otherwise, so to let the corresponding drivers to use the non-atomic
q-suffixed accessors.

Signed-off-by: Serge Semin <fancer.lancer@gmail.com>
Suggested-by: Arnd Bergmann <arnd@arndb.de>
Cc: Vadim V. Vlasov <vadim.vlasov@t-platforms.ru>
Signed-off-by: Paul Burton <paul.burton@mips.com>
Cc: Ralf Baechle <ralf@linux-mips.org>
Cc: James Hogan <jhogan@kernel.org>
Cc: Serge Semin <Sergey.Semin@t-platforms.ru>
Cc: linux-mips@vger.kernel.org
Cc: linux-kernel@vger.kernel.org
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/arch/mips/include/asm/io.h b/arch/mips/include/asm/io.h
index 29997e42480e..4597017f147b 100644
--- a/arch/mips/include/asm/io.h
+++ b/arch/mips/include/asm/io.h
@@ -462,7 +462,12 @@ __BUILD_MEMORY_PFX(, bwlq, type, 0)
 BUILDIO_MEM(b, u8)
 BUILDIO_MEM(w, u16)
 BUILDIO_MEM(l, u32)
+#ifdef CONFIG_64BIT
 BUILDIO_MEM(q, u64)
+#else
+__BUILD_MEMORY_PFX(__raw_, q, u64, 0)
+__BUILD_MEMORY_PFX(__mem_, q, u64, 0)
+#endif
 
 #define __BUILD_IOPORT_PFX(bus, bwlq, type)				\
 	__BUILD_IOPORT_SINGLE(bus, bwlq, type, 1, 0,)			\
@@ -488,12 +493,16 @@ __BUILDIO(q, u64)
 #define readb_relaxed			__relaxed_readb
 #define readw_relaxed			__relaxed_readw
 #define readl_relaxed			__relaxed_readl
+#ifdef CONFIG_64BIT
 #define readq_relaxed			__relaxed_readq
+#endif
 
 #define writeb_relaxed			__relaxed_writeb
 #define writew_relaxed			__relaxed_writew
 #define writel_relaxed			__relaxed_writel
+#ifdef CONFIG_64BIT
 #define writeq_relaxed			__relaxed_writeq
+#endif
 
 #define readb_be(addr)							\
 	__raw_readb((__force unsigned *)(addr))
@@ -516,8 +525,10 @@ __BUILDIO(q, u64)
 /*
  * Some code tests for these symbols
  */
+#ifdef CONFIG_64BIT
 #define readq				readq
 #define writeq				writeq
+#endif
 
 #define __BUILD_MEMORY_STRING(bwlq, type)				\
 									\
-- 
2.7.4

