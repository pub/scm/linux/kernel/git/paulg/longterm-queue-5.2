From 95e68e2f91796ec51135c53ca7264fa6978ec6c8 Mon Sep 17 00:00:00 2001
From: James Smart <jsmart2021@gmail.com>
Date: Mon, 11 Nov 2019 15:03:58 -0800
Subject: [PATCH] scsi: lpfc: fix inlining of lpfc_sli4_cleanup_poll_list()

commit d480e57809a043333a3b9e755c0bdd43e10a9f12 upstream.

Compilation can fail due to having an inline function reference where the
function body is not present.

Fix by removing the inline tag.

Fixes: 93a4d6f40198 ("scsi: lpfc: Add registration for CPU Offline/Online events")

Link: https://lore.kernel.org/r/20191111230401.12958-4-jsmart2021@gmail.com
Reviewed-by: Ewan D. Milne <emilne@redhat.com>
Signed-off-by: Dick Kennedy <dick.kennedy@broadcom.com>
Signed-off-by: James Smart <jsmart2021@gmail.com>
Signed-off-by: Martin K. Petersen <martin.petersen@oracle.com>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/scsi/lpfc/lpfc_crtn.h b/drivers/scsi/lpfc/lpfc_crtn.h
index 9892236ded14..4aa91d0b6291 100644
--- a/drivers/scsi/lpfc/lpfc_crtn.h
+++ b/drivers/scsi/lpfc/lpfc_crtn.h
@@ -214,7 +214,7 @@ irqreturn_t lpfc_sli_fp_intr_handler(int, void *);
 irqreturn_t lpfc_sli4_intr_handler(int, void *);
 irqreturn_t lpfc_sli4_hba_intr_handler(int, void *);
 
-inline void lpfc_sli4_cleanup_poll_list(struct lpfc_hba *phba);
+void lpfc_sli4_cleanup_poll_list(struct lpfc_hba *phba);
 int lpfc_sli4_poll_eq(struct lpfc_queue *q, uint8_t path);
 void lpfc_sli4_poll_hbtimer(struct timer_list *t);
 void lpfc_sli4_start_polling(struct lpfc_queue *q);
diff --git a/drivers/scsi/lpfc/lpfc_sli.c b/drivers/scsi/lpfc/lpfc_sli.c
index 20723e2f3d6b..c77a5fcb2383 100644
--- a/drivers/scsi/lpfc/lpfc_sli.c
+++ b/drivers/scsi/lpfc/lpfc_sli.c
@@ -14410,7 +14410,7 @@ static inline void lpfc_sli4_remove_from_poll_list(struct lpfc_queue *eq)
 		del_timer_sync(&phba->cpuhp_poll_timer);
 }
 
-inline void lpfc_sli4_cleanup_poll_list(struct lpfc_hba *phba)
+void lpfc_sli4_cleanup_poll_list(struct lpfc_hba *phba)
 {
 	struct lpfc_queue *eq, *next;
 
-- 
2.7.4

