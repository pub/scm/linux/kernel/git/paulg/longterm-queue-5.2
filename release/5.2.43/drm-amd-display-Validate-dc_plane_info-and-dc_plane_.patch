From 5ccf092c361baaed8d3869d494506ec6c1f89a03 Mon Sep 17 00:00:00 2001
From: Nicholas Kazlauskas <nicholas.kazlauskas@amd.com>
Date: Fri, 2 Aug 2019 10:31:29 -0400
Subject: [PATCH] drm/amd/display: Validate dc_plane_info and dc_plane_size in
 atomic check

commit 2cc450ceaae4febc1539975247d235eb06c11979 upstream.

[Why]
Pitch, DCC, rotation and mirroring can result in updates that are not
UPDATE_TYPE_FAST but UPDATE_TYPE_MED instead. DC needs dc_plane_info
and dc_plane_size to make this determination and we aren't currently
passing this into DC during atomic check.

Underflow (visible or non-visible) can occur if we don't validate this
correctly. This also will generally trigger p-state warnings, typically
via the cursor handler when locking.

[How]
Get the framebuffer tiling flags and generate the required structures
for DC in dm_determine_update_type_for_commit.

Signed-off-by: Nicholas Kazlauskas <nicholas.kazlauskas@amd.com>
Reviewed-by: David Francis <david.francis@amd.com>
Signed-off-by: Alex Deucher <alexander.deucher@amd.com>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/gpu/drm/amd/display/amdgpu_dm/amdgpu_dm.c b/drivers/gpu/drm/amd/display/amdgpu_dm/amdgpu_dm.c
index dda5d006ffec..5dbe0b5885eb 100644
--- a/drivers/gpu/drm/amd/display/amdgpu_dm/amdgpu_dm.c
+++ b/drivers/gpu/drm/amd/display/amdgpu_dm/amdgpu_dm.c
@@ -6553,6 +6553,12 @@ dm_determine_update_type_for_commit(struct amdgpu_display_manager *dm,
 			continue;
 
 		for_each_oldnew_plane_in_state(state, plane, old_plane_state, new_plane_state, j) {
+			const struct amdgpu_framebuffer *amdgpu_fb =
+				to_amdgpu_framebuffer(new_plane_state->fb);
+			struct dc_plane_info plane_info;
+			struct dc_flip_addrs flip_addr;
+			uint64_t tiling_flags;
+
 			new_plane_crtc = new_plane_state->crtc;
 			old_plane_crtc = old_plane_state->crtc;
 			new_dm_plane_state = to_dm_plane_state(new_plane_state);
@@ -6594,6 +6600,24 @@ dm_determine_update_type_for_commit(struct amdgpu_display_manager *dm,
 
 			updates[num_plane].scaling_info = &scaling_info;
 
+			if (amdgpu_fb) {
+				ret = get_fb_info(amdgpu_fb, &tiling_flags);
+				if (ret)
+					goto cleanup;
+
+				memset(&flip_addr, 0, sizeof(flip_addr));
+
+				ret = fill_dc_plane_info_and_addr(
+					dm->adev, new_plane_state, tiling_flags,
+					&plane_info,
+					&flip_addr.address);
+				if (ret)
+					goto cleanup;
+
+				updates[num_plane].plane_info = &plane_info;
+				updates[num_plane].flip_addr = &flip_addr;
+			}
+
 			num_plane++;
 		}
 
-- 
2.7.4

