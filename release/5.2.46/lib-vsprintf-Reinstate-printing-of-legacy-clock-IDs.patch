From 49da37c2e1ca71d8ffc26dcefe6fdafe3baf6284 Mon Sep 17 00:00:00 2001
From: Geert Uytterhoeven <geert+renesas@glider.be>
Date: Mon, 1 Jul 2019 16:00:09 +0200
Subject: [PATCH] lib/vsprintf: Reinstate printing of legacy clock IDs

commit 4ca96aa99f3e1e530f63559c0cc63ae186ecd677 upstream.

When using the legacy clock framework, clock pointers are no longer
printed as IDs, as the !CONFIG_COMMON_CLK case was accidentally
considered an error case.

Fix this by reverting to the old behavior, which allows to distinguish
clocks by ID, as the legacy clock framework does not store names with
clocks.

Fixes: 0b74d4d763fd4ee9 ("vsprintf: Consolidate handling of unknown pointer specifiers")
Link: http://lkml.kernel.org/r/20190701140009.23683-1-geert+renesas@glider.be
Cc: Sergey Senozhatsky <sergey.senozhatsky@gmail.com>
Cc: Andy Shevchenko <andriy.shevchenko@linux.intel.com>
Cc: linux-kernel@vger.kernel.org
Signed-off-by: Geert Uytterhoeven <geert+renesas@glider.be>
Signed-off-by: Petr Mladek <pmladek@suse.com>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/lib/vsprintf.c b/lib/vsprintf.c
index 8751beb4f8d0..977b9f982b38 100644
--- a/lib/vsprintf.c
+++ b/lib/vsprintf.c
@@ -1806,7 +1806,7 @@ char *clock(char *buf, char *end, struct clk *clk, struct printf_spec spec,
 #ifdef CONFIG_COMMON_CLK
 		return string(buf, end, __clk_get_name(clk), spec);
 #else
-		return error_string(buf, end, "(%pC?)", spec);
+		return ptr_to_id(buf, end, clk, spec);
 #endif
 	}
 }
-- 
2.27.0

